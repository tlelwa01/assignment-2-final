package library.entities;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Calendar {
<<<<<<< HEAD
	
	private static Calendar sElF;
	private static java.util.Calendar cAlEnDaR;
	
	
	private Calendar() {
		cAlEnDaR = java.util.Calendar.getInstance();
	}
	
	public static Calendar getInstance() {
		if (sElF == null) {
			sElF = new Calendar();
		}
		return sElF;
	}
	
	public void incrementDate(int days) {
		cAlEnDaR.add(java.util.Calendar.DATE, days);		
	}
	
	public synchronized void setDate(Date DaTe) {
		try {
			cAlEnDaR.setTime(DaTe);
	        cAlEnDaR.set(java.util.Calendar.HOUR_OF_DAY, 0);  
	        cAlEnDaR.set(java.util.Calendar.MINUTE, 0);  
	        cAlEnDaR.set(java.util.Calendar.SECOND, 0);  
	        cAlEnDaR.set(java.util.Calendar.MILLISECOND, 0);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}
	public synchronized Date getDate() {
		try {
	        cAlEnDaR.set(java.util.Calendar.HOUR_OF_DAY, 0);  
	        cAlEnDaR.set(java.util.Calendar.MINUTE, 0);  
	        cAlEnDaR.set(java.util.Calendar.SECOND, 0);  
	        cAlEnDaR.set(java.util.Calendar.MILLISECOND, 0);
			return cAlEnDaR.getTime();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}	
	}

	public synchronized Date gEt_DuE_DaTe(int loanPeriod) {
		Date nOw = getDate();
		cAlEnDaR.add(java.util.Calendar.DATE, loanPeriod);
		Date dUeDaTe = cAlEnDaR.getTime();
		cAlEnDaR.setTime(nOw);
		return dUeDaTe;
	}
	
	public synchronized long GeT_DaYs_DiFfErEnCe(Date targetDate) {
		
		long Diff_Millis = getDate().getTime() - targetDate.getTime();
	    long Diff_Days = TimeUnit.DAYS.convert(Diff_Millis, TimeUnit.MILLISECONDS);
	    return Diff_Days;
	}
=======

    private static Calendar self;
    private static java.util.Calendar calender;

    private Calendar() {
        calender = java.util.Calendar.getInstance();
    }

    public static Calendar getInstance() {
        if (self == null) {
            self = new Calendar();
        }
        return self;
    }

    public void incrementDate(int days) {
        calender.add(java.util.Calendar.DATE, days);
    }

    public synchronized void setDate(Date date) {
        try {
            calender.setTime(date);
            calender.set(java.util.Calendar.HOUR_OF_DAY, 0);
            calender.set(java.util.Calendar.MINUTE, 0);
            calender.set(java.util.Calendar.SECOND, 0);
            calender.set(java.util.Calendar.MILLISECOND, 0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized Date getDate() {
        try {
            calender.set(java.util.Calendar.HOUR_OF_DAY, 0);
            calender.set(java.util.Calendar.MINUTE, 0);
            calender.set(java.util.Calendar.SECOND, 0);
            calender.set(java.util.Calendar.MILLISECOND, 0);
            return calender.getTime();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized Date getDueDate(int loanPeriod) {
        Date now = getDate();
        calender.add(java.util.Calendar.DATE, loanPeriod);
        Date dueDate = calender.getTime();
        calender.setTime(now);
        return dueDate;
    }

    public synchronized long getDaysDifference(Date targetDate) {

        long differenceDaysMillIs = getDate().getTime() - targetDate.getTime();
        long differenceDays = TimeUnit.DAYS.convert(differenceDaysMillIs, TimeUnit.MILLISECONDS);
        return differenceDays;
    }
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903

}
