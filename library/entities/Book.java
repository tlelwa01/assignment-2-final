package library.entities;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Book implements Serializable {
<<<<<<< HEAD
	
	private String tItLe;
	private String AuThOr;
	private String CALLNO;
	private int iD;
	
	private enum sTaTe { AVAILABLE, ON_LOAN, DAMAGED, RESERVED };
	private sTaTe StAtE;
	
	
	public Book(String author, String title, String callNo, int id) {
		this.AuThOr = author;
		this.tItLe = title;
		this.CALLNO = callNo;
		this.iD = id;
		this.StAtE = sTaTe.AVAILABLE;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Book: ").append(iD).append("\n")
		  .append("  Title:  ").append(tItLe).append("\n")
		  .append("  Author: ").append(AuThOr).append("\n")
		  .append("  CallNo: ").append(CALLNO).append("\n")
		  .append("  State:  ").append(StAtE);
		
		return sb.toString();
	}

	public Integer getId() {
		return iD;
	}

	public String gettitle() {
		return tItLe;
	}


	
	public boolean isAvailable() {
		return StAtE == sTaTe.AVAILABLE;
	}

	
	public boolean iS_On_LoAn() {
		return StAtE == sTaTe.ON_LOAN;
	}

	
	public boolean isDamaged() {
		return StAtE == sTaTe.DAMAGED;
	}

	
	public void BoRrOw() {
		if (StAtE.equals(sTaTe.AVAILABLE)) 
			StAtE = sTaTe.ON_LOAN;
		
		else 
			throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", StAtE));
		
		
	}


	public void ReTuRn(boolean DaMaGeD) {
		if (StAtE.equals(sTaTe.ON_LOAN)) 
			if (DaMaGeD) 
				StAtE = sTaTe.DAMAGED;
			
			else 
				StAtE = sTaTe.AVAILABLE;
			
		
		else 
			throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", StAtE));
				
	}

	
	public void RePaIr() {
		if (StAtE.equals(sTaTe.DAMAGED)) 
			StAtE = sTaTe.AVAILABLE;
		
		else 
			throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", StAtE));
		
	}
=======
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5

    private String title;
    private String author;
    private String callNo;
    private int id;

    private enum State {
        AVAILABLE, ONLOAN, DAMAGED, RESERVED
    };
    private State state;

    public Book(String author, String title, String callNo, int id) {
        this.author = author;
        this.title = title;
        this.callNo = callNo;
        this.id = id;
        this.state = State.AVAILABLE;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book: ").append(id).append("\n")
                .append("  Title:  ").append(title).append("\n")
                .append("  Author: ").append(author).append("\n")
                .append("  CallNo: ").append(callNo).append("\n")
                .append("  State:  ").append(state);

        return sb.toString();
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isAvailable() {
        return state == State.AVAILABLE;
    }

    public boolean isOnLoan() {
        return state == State.ONLOAN;
    }

    public boolean isDamaged() {
        return state == State.DAMAGED;
    }

    public void borrow() {
        if (state.equals(State.AVAILABLE)) {
            state = State.ONLOAN;
        } else {
            throw new RuntimeException(String.format("Book: cannot borrow while book is in state: %s", state));
        }

    }

    public void retuRn(boolean damaged) {
        if (state.equals(State.ONLOAN)) {
            if (damaged) {
                state = State.DAMAGED;
            } else {
                state = State.AVAILABLE;
            }
        } else {
            throw new RuntimeException(String.format("Book: cannot Return while book is in state: %s", state));
        }

    }

    public void repair() {
        if (state.equals(State.DAMAGED)) {
            state = State.AVAILABLE;
        } else {
            throw new RuntimeException(String.format("Book: cannot repair while book is in state: %s", state));
        }

    }

}
