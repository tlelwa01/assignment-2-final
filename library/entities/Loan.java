package library.entities;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class Loan implements Serializable {
	
	public static enum loanState { CURRENT, OVER_DUE, DISCHARGED };
	
	private int loanId;
	private Book book;
	private Member member;
	private Date date;
	private loanState state;

	
	public Loan(int loanId, Book book, Member mEmBeR, Date dueDate) {
		this.loanId = loanId;
		this.book = book;
		this.member = member;
		this.date = dueDate;
		this.state = loanState.CURRENT;
	}

	
<<<<<<< HEAD
	public void checkOverDue() {
		if (state == loanState.CURRENT &&
			Calendar.getInstance().getDate().after(date)) 
			this.state = loanState.OVER_DUE;			
=======
	public void cHeCk_OvEr_DuE() {
		if (StAtE == lOaN_sTaTe.CURRENT &&
			Calendar.getInstance().getDate().after(DaTe)) 
			this.StAtE = lOaN_sTaTe.OVER_DUE;			
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
		
	}

	
	public boolean isOverDue() {
<<<<<<< HEAD
		return state == loanState.OVER_DUE;
=======
		return StAtE == lOaN_sTaTe.OVER_DUE;
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
	}

	
	public Integer getId() {
		return loanId;
	}


	public Date getDueDate() {
		return date;
	}
	
	
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		StringBuilder sb = new StringBuilder();
<<<<<<< HEAD
		sb.append("Loan:  ").append(loanId).append("\n")
		  .append("  Borrower ").append(member.getId()).append(" : ")
		  .append(member.GeT_LaSt_NaMe()).append(", ").append(member.getFirstName()).append("\n")
		  .append("  Book ").append(book.getId()).append(" : " )
		  .append(book.gettitle()).append("\n")
		  .append("  DueDate: ").append(sdf.format(date)).append("\n")
		  .append("  State: ").append(state);		
=======
		sb.append("Loan:  ").append(LoAn_Id).append("\n")
<<<<<<< HEAD
		  .append("  Borrower ").append(MeMbEr.getId()).append(" : ")
		  .append(MeMbEr.getLastName()).append(", ").append(MeMbEr.getFirstName()).append("\n")
		  .append("  Book ").append(BoOk.gEtId()).append(" : " )
		  .append(BoOk.gEtTiTlE()).append("\n")
=======
		  .append("  Borrower ").append(MeMbEr.GeT_ID()).append(" : ")
		  .append(MeMbEr.GeT_LaSt_NaMe()).append(", ").append(MeMbEr.GeT_FiRsT_NaMe()).append("\n")
		  .append("  Book ").append(BoOk.getId()).append(" : " )
		  .append(BoOk.getTitle()).append("\n")
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
		  .append("  DueDate: ").append(sdf.format(DaTe)).append("\n")
		  .append("  State: ").append(StAtE);		
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
		return sb.toString();
	}


	public Member getMember() {
		return member;
	}


	public Book getBook() {
		return book;
	}


	public void discharge() {
		state = loanState.DISCHARGED;		
	}

}
