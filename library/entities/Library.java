package library.entities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("serial")
public class Library implements Serializable {
<<<<<<< HEAD

    private static final String LIBRARY_FILE = "library.obj";
    private static final int LOAN_LIMIT = 2;
    private static final int LOAN_PERIOD = 2;
    private static final double FINE_PER_DAY = 1.0;
    private static final double MAX_FINES_OWED = 1.0;
    private static final double DAMAGE_FEE = 2.0;

    private static Library self;
    private int bookId;
    private int memberID;
    private int loanId;
    private Date loanDate;

    private Map<Integer, Book> catalog;
    private Map<Integer, Member> members;
    private Map<Integer, Loan> loans;
    private Map<Integer, Loan> currentLoans;
    private Map<Integer, Book> damagedBooks;

    private Library() {
        catalog = new HashMap<>();
        members = new HashMap<>();
        loans = new HashMap<>();
        currentLoans = new HashMap<>();
        damagedBooks = new HashMap<>();
        bookId = 1;
        memberID = 1;
        loanId = 1;
    }

    public static synchronized Library getInstance() {
        if (self == null) {
            Path PATH = Paths.get(LIBRARY_FILE);
            if (Files.exists(PATH)) {
                try ( ObjectInputStream libraryFile = new ObjectInputStream(new FileInputStream(LIBRARY_FILE));) {

                    self = (Library) libraryFile.readObject();
                    Calendar.getInstance().setDate(self.loanDate);
                    libraryFile.close();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            } else {
                self = new Library();
            }
        }
        return self;
    }

    public static synchronized void save() {
        if (self != null) {
            self.loanDate = Calendar.getInstance().getDate();
            try ( ObjectOutputStream LiBrArY_fIlE = new ObjectOutputStream(new FileOutputStream(LIBRARY_FILE));) {
                LiBrArY_fIlE.writeObject(self);
                LiBrArY_fIlE.flush();
                LiBrArY_fIlE.close();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public int getBookId() {
        return bookId;
    }

    public int getMenberId() {
        return memberID;
    }

    private int getNextBookId() {
        return bookId++;
    }

    private int getNextMemberId() {
        return memberID++;
    }

    private int getNextLoanId() {
        return loanId++;
    }

    public List<Member> listMembers() {
        return new ArrayList<Member>(members.values());
    }

    public List<Book> listBooks() {
        return new ArrayList<Book>(catalog.values());
    }

    public List<Loan> listCurrentLoans() {
        return new ArrayList<Loan>(currentLoans.values());
    }

    public Member addMember(String lastName, String firstName, String email, int phoneNo) {
        Member member = new Member(lastName, firstName, email, phoneNo, getNextMemberId());
        members.put(member.getId(), member);
        return member;
    }

    public Book addBook(String a, String t, String c) {
        Book b = new Book(a, t, c, getNextBookId());
        catalog.put(b.getId(), b);
        return b;
    }

    public Member getMember(int memberId) {
        if (members.containsKey(memberId)) {
            return members.get(memberId);
        }
        return null;
    }

    public Book getBook(int bookId) {
        if (catalog.containsKey(bookId)) {
            return catalog.get(bookId);
        }
        return null;
    }

    public int getLoanLimit() {
        return LOAN_LIMIT;
    }

    public boolean canMemberBorrow(Member member) {
        if (member.getNumberOfCurrentLoans() == LOAN_LIMIT) {
            return false;
        }

        if (member.finesOwed() >= MAX_FINES_OWED) {
            return false;
        }

        for (Loan loan : member.getLoans()) {
            if (loan.isOverDue()) {
                return false;
            }
        }

        return true;
    }

    public int getNumberOfLoansRemainingForMember(Member member) {
        return LOAN_LIMIT - member.getNumberOfCurrentLoans();
    }

    public Loan issueLoan(Book book, Member member) {
        Date dueDate = Calendar.getInstance().getDueDate(LOAN_PERIOD);
        Loan loan = new Loan(getNextLoanId(), book, member, dueDate);
        member.takeOutLoan(loan);
        book.borrow();
        loans.put(loan.getId(), loan);
        currentLoans.put(book.getId(), loan);
        return loan;
    }

    public Loan getLoanByBookId(int bookId) {
        if (currentLoans.containsKey(bookId)) {
            return currentLoans.get(bookId);
        }

        return null;
    }

    public double calculateOverDueFine(Loan loAn) {
        if (loAn.isOverDue()) {
            long daysOverDue = Calendar.getInstance().getDaysDifference(loAn.getDueDate());
            double fInE = daysOverDue * FINE_PER_DAY;
            return fInE;
        }
        return 0.0;
    }

    public void dischargeLoan(Loan currentLoan, boolean isDamaged) {
        Member member = currentLoan.getMember();
        Book book = currentLoan.getBook();

        double oVeR_DuE_FiNe = calculateOverDueFine(currentLoan);
        member.addFine(oVeR_DuE_FiNe);

        member.dischargeLoan(currentLoan);
        book.retuRn(isDamaged);
        if (isDamaged) {
            member.addFine(DAMAGE_FEE);
            damagedBooks.put(book.getId(), book);
        }
        currentLoan.discharge();
        currentLoans.remove(book.getId());
    }

    public void checkCurrentLoans() {
        for (Loan lOaN : currentLoans.values()) {
            lOaN.checkOverDue();
        }

    }

    public void repairBook(Book currentBook) {
        if (damagedBooks.containsKey(currentBook.getId())) {
            currentBook.repair();
            damagedBooks.remove(currentBook.getId());
        } else {
            throw new RuntimeException("Library: repairBook: book is not damaged");
        }

    }

=======
	
	private static final String lIbRaRyFiLe = "library.obj";
	private static final int lOaNlImIt = 2;
	private static final int loanPeriod = 2;
	private static final double FiNe_PeR_DaY = 1.0;
	private static final double maxFinesOwed = 1.0;
	private static final double damageFee = 2.0;
	
	private static Library SeLf;
	private int bOoK_Id;
	private int mEmBeR_Id;
	private int lOaN_Id;
	private Date lOaN_DaTe;
	
	private Map<Integer, Book> CaTaLoG;
	private Map<Integer, Member> MeMbErS;
	private Map<Integer, Loan> LoAnS;
	private Map<Integer, Loan> CuRrEnT_LoAnS;
	private Map<Integer, Book> DaMaGeD_BoOkS;
	

	private Library() {
		CaTaLoG = new HashMap<>();
		MeMbErS = new HashMap<>();
		LoAnS = new HashMap<>();
		CuRrEnT_LoAnS = new HashMap<>();
		DaMaGeD_BoOkS = new HashMap<>();
		bOoK_Id = 1;
		mEmBeR_Id = 1;		
		lOaN_Id = 1;		
	}

	
	public static synchronized Library GeTiNsTaNcE() {		
		if (SeLf == null) {
			Path PATH = Paths.get(lIbRaRyFiLe);			
			if (Files.exists(PATH)) {	
				try (ObjectInputStream LiBrArY_FiLe = new ObjectInputStream(new FileInputStream(lIbRaRyFiLe));) {
			    
					SeLf = (Library) LiBrArY_FiLe.readObject();
					Calendar.getInstance().setDate(SeLf.lOaN_DaTe);
					LiBrArY_FiLe.close();
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else SeLf = new Library();
		}
		return SeLf;
	}

	
	public static synchronized void SaVe() {
		if (SeLf != null) {
			SeLf.lOaN_DaTe = Calendar.getInstance().getDate();
			try (ObjectOutputStream LiBrArY_fIlE = new ObjectOutputStream(new FileOutputStream(lIbRaRyFiLe));) {
				LiBrArY_fIlE.writeObject(SeLf);
				LiBrArY_fIlE.flush();
				LiBrArY_fIlE.close();	
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	
	public int gEt_BoOkId() {
		return bOoK_Id;
	}
	
	
	public int gEt_MeMbEr_Id() {
		return mEmBeR_Id;
	}
	
	
	private int gEt_NeXt_BoOk_Id() {
		return bOoK_Id++;
	}

	
	private int gEt_NeXt_MeMbEr_Id() {
		return mEmBeR_Id++;
	}

	
	private int gEt_NeXt_LoAn_Id() {
		return lOaN_Id++;
	}

	
	public List<Member> lIsT_MeMbErS() {		
		return new ArrayList<Member>(MeMbErS.values()); 
	}


	public List<Book> lIsT_BoOkS() {		
		return new ArrayList<Book>(CaTaLoG.values()); 
	}


	public List<Loan> lISt_CuRrEnT_LoAnS() {
		return new ArrayList<Loan>(CuRrEnT_LoAnS.values());
	}


	public Member aDd_MeMbEr(String lastName, String firstName, String email, int phoneNo) {		
		Member member = new Member(lastName, firstName, email, phoneNo, gEt_NeXt_MeMbEr_Id());
		MeMbErS.put(member.getId(), member);		
		return member;
	}

	
	public Book aDd_BoOk(String a, String t, String c) {		
		Book b = new Book(a, t, c, gEt_NeXt_BoOk_Id());
		CaTaLoG.put(b.getId(), b);		
		return b;
	}

	
	public Member getMember(int memberId) {
		if (MeMbErS.containsKey(memberId)) 
			return MeMbErS.get(memberId);
		return null;
	}

	
	public Book getBook(int bookId) {
		if (CaTaLoG.containsKey(bookId)) 
			return CaTaLoG.get(bookId);		
		return null;
	}

	
	public int gEt_LoAn_LiMiT() {
		return lOaNlImIt;
	}

	
	public boolean cAn_MeMbEr_BoRrOw(Member member) {		
		if (member.getNumberOfCurrentLoans() == lOaNlImIt ) 
			return false;
				
		if (member.finesOwned() >= maxFinesOwed) 
			return false;
				
<<<<<<< HEAD
		for (Loan loan : member.getLoans()) 
			if (loan.Is_OvEr_DuE()) 
=======
		for (Loan loan : member.GeT_LoAnS()) 
			if (loan.isOverDue()) 
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
				return false;
			
		return true;
	}

	
	public int gEt_NuMbEr_Of_LoAnS_ReMaInInG_FoR_MeMbEr(Member MeMbEr) {		
		return lOaNlImIt - MeMbEr.getNumberOfCurrentLoans();
	}

	
	public Loan iSsUe_LoAn(Book book, Member member) {
		Date dueDate = Calendar.getInstance().getDueDate(loanPeriod);
		Loan loan = new Loan(gEt_NeXt_LoAn_Id(), book, member, dueDate);
<<<<<<< HEAD
		member.takeOutLoan(loan);
		book.BoRrOw();
=======
		member.TaKe_OuT_LoAn(loan);
		book.borrow();
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
		LoAnS.put(loan.GeT_Id(), loan);
		CuRrEnT_LoAnS.put(book.getId(), loan);
		return loan;
	}
	
	
	public Loan getLoanByBookId(int bookId) {
		if (CuRrEnT_LoAnS.containsKey(bookId)) 
			return CuRrEnT_LoAnS.get(bookId);
		
		return null;
	}

	
	public double CaLcUlAtE_OvEr_DuE_FiNe(Loan LoAn) {
		if (LoAn.isOverDue()) {
			long DaYs_OvEr_DuE = Calendar.getInstance().getDaysDiffernce(LoAn.GeT_DuE_DaTe());
			double fInE = DaYs_OvEr_DuE * FiNe_PeR_DaY;
			return fInE;
		}
		return 0.0;		
	}


	public void DiScHaRgE_LoAn(Loan cUrReNt_LoAn, boolean iS_dAmAgEd) {
		Member mEmBeR = cUrReNt_LoAn.GeT_MeMbEr();
		Book bOoK  = cUrReNt_LoAn.GeT_BoOk();
		
		double oVeR_DuE_FiNe = CaLcUlAtE_OvEr_DuE_FiNe(cUrReNt_LoAn);
		mEmBeR.addFine(oVeR_DuE_FiNe);	
		
		mEmBeR.disChargeLoan(cUrReNt_LoAn);
		bOoK.ReTuRn(iS_dAmAgEd);
		if (iS_dAmAgEd) {
<<<<<<< HEAD
			mEmBeR.addFine(damageFee);
			DaMaGeD_BoOkS.put(bOoK.gEtId(), bOoK);
=======
			mEmBeR.AdD_FiNe(damageFee);
			DaMaGeD_BoOkS.put(bOoK.getId(), bOoK);
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
		}
		cUrReNt_LoAn.DiScHaRgE();
		CuRrEnT_LoAnS.remove(bOoK.getId());
	}


	public void cHeCk_CuRrEnT_LoAnS() {
		for (Loan lOaN : CuRrEnT_LoAnS.values()) 
			lOaN.cHeCk_OvEr_DuE();
				
	}


	public void RePaIr_BoOk(Book cUrReNt_BoOk) {
		if (DaMaGeD_BoOkS.containsKey(cUrReNt_BoOk.getId())) {
			cUrReNt_BoOk.repair();
			DaMaGeD_BoOkS.remove(cUrReNt_BoOk.getId());
		}
		else 
			throw new RuntimeException("Library: repairBook: book is not damaged");
		
		
	}
	
	
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
}
