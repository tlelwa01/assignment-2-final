package library;
<<<<<<< HEAD

=======
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
import java.text.SimpleDateFormat;
import java.util.Scanner;

import library.borrowbook.BorrowBookUI;
<<<<<<< HEAD
import library.borrowbook.BorrowBookControl;
=======
import library.borrowbook.bORROW_bOOK_cONTROL;
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
import library.entities.Book;
import library.entities.Calendar;
import library.entities.Library;
import library.entities.Loan;
import library.entities.Member;
import library.fixbook.FixBookUI;
<<<<<<< HEAD
import library.fixbook.FixBookControl;
import library.payfine.PayFineControl;
import library.payfine.PayFineUI;
import library.payfine.PayFineControl;
import library.returnBook.ReturnBookControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;

public class Main {

    private static Scanner input;
    private static Library library;
    private static String menu;
    private static Calendar calender;
    private static SimpleDateFormat simpleDateFormat;

    private static String getMenu() {
        StringBuilder sb = new StringBuilder();

        sb.append("\nLibrary Main Menu\n\n")
                .append("  M  : add member\n")
                .append("  LM : list members\n")
                .append("\n")
                .append("  B  : add book\n")
                .append("  LB : list books\n")
                .append("  FB : fix books\n")
                .append("\n")
                .append("  L  : take out a loan\n")
                .append("  R  : return a loan\n")
                .append("  LL : list loans\n")
                .append("\n")
                .append("  P  : pay fine\n")
                .append("\n")
                .append("  T  : increment date\n")
                .append("  Q  : quit\n")
                .append("\n")
                .append("Choice : ");

        return sb.toString();
    }

    public static void main(String[] args) {
        try {
            input = new Scanner(System.in);
            library = Library.getInstance();
            calender = Calendar.getInstance();
            simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

            for (Member m : library.listMembers()) {
                setOutput(m);
            }
            setOutput(" ");
            for (Book b : library.listBooks()) {
                setOutput(b);
            }

            menu = getMenu();

            boolean e = false;

            while (!e) {

                setOutput("\n" + simpleDateFormat.format(calender.getDate()));
                String c = getInput(menu);

                switch (c.toUpperCase()) {

                    case "M":
                        addMember();
                        break;

                    case "LM":
                        listMembers();
                        break;

                    case "B":
                        addBook();
                        break;

                    case "LB":
                        listBooks();
                        break;

                    case "FB":
                        fixBooks();
                        break;

                    case "L":
                        borrowBook();
                        break;

                    case "R":
                        returnBook();
                        break;

                    case "LL":
                        listCurrentLoans();
                        break;

                    case "P":
                        payFines();
                        break;

                    case "T":
                        incrementDate();
                        break;

                    case "Q":
                        e = true;
                        break;

                    default:
                        setOutput("\nInvalid option\n");
                        break;
                }

                Library.save();
            }
        } catch (RuntimeException e) {
              setOutput(e);
        }
        setOutput("\nEnded\n");
    }

    private static void payFines() {
        new PayFineUI(new PayFineControl()).run();
    }

    private static void listCurrentLoans() {
        setOutput("");
        for (Loan loan : library.listCurrentLoans()) {
            setOutput(loan + "\n");
        }
    }

    private static void listBooks() {
        setOutput("");
        for (Book book : library.listBooks()) {
            setOutput(book + "\n");
        }
    }

    private static void listMembers() {
        setOutput("");
        for (Member member : library.listMembers()) {
            setOutput(member + "\n");
        }
    }

    private static void borrowBook() {
        new BorrowBookUI(new BorrowBookControl()).run();
    }

    private static void returnBook() {
        new ReturnBookUI(new ReturnBookControl()).run();
    }

    private static void fixBooks() {
        new FixBookUI(new FixBookControl()).run();
    }

    private static void incrementDate() {
        try {
            int days = Integer.valueOf(getInput("Enter number of days: ")).intValue();
            calender.incrementDate(days);
            library.checkCurrentLoans();
            setOutput(simpleDateFormat.format(calender.getDate()));

        } catch (NumberFormatException e) {
            setOutput("\nInvalid number of days\n");
        }
    }

    private static void addBook() {

        String  author = getInput("Enter author: ");
        String title = getInput("Enter title: ");
        String callNumber = getInput("Enter call number: ");
        Book book = library.addBook(author, title, callNumber);
        setOutput("\n" + book + "\n");

    }

    private static void addMember() {
        try {
            String lastName = getInput("Enter last name: ");
            String firstName = getInput("Enter first name: ");
            String emailAddress = getInput("Enter email address: ");
            int phoneNumber = Integer.valueOf(getInput("Enter phone number: ")).intValue();
            Member member = library.addMember(lastName, firstName, emailAddress, phoneNumber);
            setOutput("\n" + member + "\n");

        } catch (NumberFormatException e) {
            setOutput("\nInvalid phone number\n");
        }

    }

    private static String getInput(String prompt) {
        System.out.print(prompt);
        return input.nextLine();
    }

    private static void setOutput(Object object) {
        System.out.println(object);
    }

=======
<<<<<<< HEAD
import library.fixbook.fixBookControl;
import library.payfine.PayFineUI;
import library.payfine.payFineControl;
import library.returnBook.ReturnBookUI;
import library.returnBook.rETURN_bOOK_cONTROL;
=======
import library.fixbook.fIX_bOOK_cONTROL;
import library.payfine.PayFineUI;
import library.payfine.pAY_fINE_cONTROL;
import library.returnBook.ReturnBookUI;
import library.returnBook.ReturnBookControl;
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5


public class Main {
	
	private static Scanner IN;
	private static Library LIB;
	private static String MENU;
	private static Calendar CAL;
	private static SimpleDateFormat SDF;
	
	
	private static String Get_menu() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("\nLibrary Main Menu\n\n")
		  .append("  M  : add member\n")
		  .append("  LM : list members\n")
		  .append("\n")
		  .append("  B  : add book\n")
		  .append("  LB : list books\n")
		  .append("  FB : fix books\n")
		  .append("\n")
		  .append("  L  : take out a loan\n")
		  .append("  R  : return a loan\n")
		  .append("  LL : list loans\n")
		  .append("\n")
		  .append("  P  : pay fine\n")
		  .append("\n")
		  .append("  T  : increment date\n")
		  .append("  Q  : quit\n")
		  .append("\n")
		  .append("Choice : ");
		  
		return sb.toString();
	}


	public static void main(String[] args) {		
		try {			
			IN = new Scanner(System.in);
			LIB = Library.GeTiNsTaNcE();
<<<<<<< HEAD
			CAL = Calendar.gEtInStAnCe();
=======
			CAL = Calendar.getInstance();
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
			SDF = new SimpleDateFormat("dd/MM/yyyy");
	
			for (Member m : LIB.lIsT_MeMbErS()) {
				output(m);
			}
			output(" ");
			for (Book b : LIB.lIsT_BoOkS()) {
				output(b);
			}
						
			MENU = Get_menu();
			
			boolean e = false;
			
			while (!e) {
				
<<<<<<< HEAD
				output("\n" + SDF.format(CAL.gEt_DaTe()));
=======
				output("\n" + SDF.format(CAL.getDate()));
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
				String c = input(MENU);
				
				switch (c.toUpperCase()) {
				
				case "M": 
					ADD_MEMBER();
					break;
					
				case "LM": 
					LIST_MEMBERS();
					break;
					
				case "B": 
					ADD_BOOK();
					break;
					
				case "LB": 
					LIST_BOOKS();
					break;
					
				case "FB": 
					FIX_BOOKS();
					break;
					
				case "L": 
					BORROW_BOOK();
					break;
					
				case "R": 
					RETURN_BOOK();
					break;
					
				case "LL": 
					LIST_CURRENT_LOANS();
					break;
					
				case "P": 
					PAY_FINES();
					break;
					
				case "T": 
					INCREMENT_DATE();
					break;
					
				case "Q": 
					e = true;
					break;
					
				default: 
					output("\nInvalid option\n");
					break;
				}
				
				Library.SaVe();
			}			
		} catch (RuntimeException e) {
			output(e);
		}		
		output("\nEnded\n");
	}	

	
	private static void PAY_FINES() {
<<<<<<< HEAD
		new PayFineUI(new payFineControl()).run();		
=======
		new PayFineUI(new pAY_fINE_cONTROL()).RuN();		
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
	}


	private static void LIST_CURRENT_LOANS() {
		output("");
		for (Loan loan : LIB.lISt_CuRrEnT_LoAnS()) {
			output(loan + "\n");
		}		
	}



	private static void LIST_BOOKS() {
		output("");
		for (Book book : LIB.lIsT_BoOkS()) {
			output(book + "\n");
		}		
	}



	private static void LIST_MEMBERS() {
		output("");
		for (Member member : LIB.lIsT_MeMbErS()) {
			output(member + "\n");
		}		
	}



	private static void BORROW_BOOK() {
		new BorrowBookUI(new bORROW_bOOK_cONTROL()).RuN();		
	}


	private static void RETURN_BOOK() {
<<<<<<< HEAD
		new ReturnBookUI(new rETURN_bOOK_cONTROL()).RuN();		
=======
		new ReturnBookUI(new ReturnBookControl()).run();		
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
	}


	private static void FIX_BOOKS() {
<<<<<<< HEAD
		new FixBookUI(new fixBookControl()).run();		
=======
		new FixBookUI(new fIX_bOOK_cONTROL()).RuN();		
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
	}


	private static void INCREMENT_DATE() {
		try {
			int days = Integer.valueOf(input("Enter number of days: ")).intValue();
			CAL.incrementDate(days);
			LIB.cHeCk_CuRrEnT_LoAnS();
<<<<<<< HEAD
			output(SDF.format(CAL.gEt_DaTe()));
=======
			output(SDF.format(CAL.getDate()));
>>>>>>> b9cc9084c7d51c22b18e7e3954c575e7dfa0f8d5
			
		} catch (NumberFormatException e) {
			 output("\nInvalid number of days\n");
		}
	}


	private static void ADD_BOOK() {
		
		String AuThOr = input("Enter author: ");
		String TiTlE  = input("Enter title: ");
		String CaLl_NuMbEr = input("Enter call number: ");
		Book BoOk = LIB.aDd_BoOk(AuThOr, TiTlE, CaLl_NuMbEr);
		output("\n" + BoOk + "\n");
		
	}

	
	private static void ADD_MEMBER() {
		try {
			String LaSt_NaMe = input("Enter last name: ");
			String FiRsT_NaMe  = input("Enter first name: ");
			String EmAiL_AdDrEsS = input("Enter email address: ");
			int PhOnE_NuMbEr = Integer.valueOf(input("Enter phone number: ")).intValue();
			Member MeMbEr = LIB.aDd_MeMbEr(LaSt_NaMe, FiRsT_NaMe, EmAiL_AdDrEsS, PhOnE_NuMbEr);
			output("\n" + MeMbEr + "\n");
			
		} catch (NumberFormatException e) {
			 output("\nInvalid phone number\n");
		}
		
	}


	private static String input(String prompt) {
		System.out.print(prompt);
		return IN.nextLine();
	}
	
	
	
	private static void output(Object object) {
		System.out.println(object);
	}

	
>>>>>>> b51cd744e69b06a6ca131c0b70a1cd62161e8903
}
